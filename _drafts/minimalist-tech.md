+++
title = "Practical NixOS Expressions"
description = "How to apply expressions in daily use."
category = "NixOS"
tags = ["nixos", "deployment", "expressions"]
+++
Imagine using a computer that you own.

In my experience meeting various kinds of people, there are two very prominent realities that people live in; the creator, and the consumer. If there's a third, it's when a person is trapped in the consumer mindset and trying their best to switch sides to a creator.


