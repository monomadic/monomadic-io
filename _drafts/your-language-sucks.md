
Your project sucks.

Your language? It sucks.

Your package manager? Wow. Worst.

Your deployment strategy? With the full time devs and the CI/CD workflows and the cloud-based acronym thingy? I'm not even going to bother.

Okay, I should preface. There are some development flows today which don't suck as hard as others, but really, it's just a matter of degree.

Before you get dismiss me as "that arrogant purist solo programmer", I'm trying to raise a point that really, we as an industry, and individuals, need to move beyond. There's a plague of short term thinking and follow-the-leader culture when it comes to the environments we suffer, and it **does** have a remedy.

Around two years ago, I got into [NixOS]. I've been [functionally-slanted] for a while now, and prior to that still understood and agreed with the [complexity-debate]. I'm into [suckless] style, if not implementation, I run my own window manager cobbled together from a few c programs.

Sometimes, I need to write packages for my install, particularly if I want to try out an application and it isn't in the [nixpkgs] repository. The thing is, NixOS doesn't embrace mutable operating system architectures, so generally if you want to really have a program as a part of your work, you'll need to dig a little bit into how it was made.

Having done this to [dozens] of programs now
