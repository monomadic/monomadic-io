+++
title = "Creating a Blockchain DNS client"
description = "Exploring web3 DNS by hand"
category = "Blockchain"
tags = ["crypto", "dns", "domains", "web3"]
+++

Programming today almost always trends toward heavily abstracted. I've written about this kind of thing before. Things have gotten better over the years from the Ruby on Rails days, but we still have javascript.

Blockchain has not escaped the trend toward abstraction unfortunately, though in some areas it is not so bad. Ethereum is not one of those areas.

I'll skip the lecture on why languages like solidity are terrible for writing finance applications on, and instead focus on trying to make do and not make those concessions myself. To start with, I'd like to understand how to interact with Ethereum without a million languages on top of each other until I no longer understand what the blockchain underneath is doing or how it is constructed.

I settled on a simple project, having bought a few blockchain domains and I'm quite keenly interested in this area. Writing a command line tool to resolve a DNS entry from the ethereum blockchain sounds like a perfect way to easy into actually understanding what Ethereum is doing under all of the npm web3 libraries and so on.

## Structure of the ethereum blockchain

Nodes talk to each other over JSON RPC.

First thing to do is discover or find some nodes. We do this using the [discovery protocol](https://github.com/ethereum/devp2p/blob/master/discv4.md), which is essentially the kademlia peer discovery protocol.
