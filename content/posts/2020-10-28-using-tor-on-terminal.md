+++
title = "Accessing Secret GIT Repositories On TOR."
description = "Set up your terminal environment to access tor."
category = "TOR"
tags = ["nixos", "nix", "tor", "onion", "git"]
+++

The github acquisition by microsoft recently has led to predictable moves toward censorship and lockin. I looked the other way on this because, well, network effect. But recently google issued a DMCA takedown for `youtube-dl`, a tool that helped many other tools liberate content from the content creators dystopian nightmare that is the youtube platform.

They have broken no laws, and even if they have, are they laws we want to tolerate and/or encourage?

During this takedown, I did notice an interesting comment. Someone posted a git clone to a TOR node:

``` bash
git clone http://dacxzjk3kq5mmepbdd3ai2ifynlzxsnpl2cnkfhridqfywihrfftapid.onion/shawn.webb/youtube-dl.git
```

This intrigued me, as previously to this I had always been using TOR via a browser (`nix-shell --packages torbrowser --run "torbrowser dark.fail"`), and while [decentralised git](https://docs.ipfs.io/how-to/host-git-style-repo/) is something I'd read and thought about, anonymized git is new to me.

## Setup

We need to relay requests through TOR, so TOR Proxy is required.

Once that's set up, we simply need to configure the repo to always use the proxy.

``` bash
git -c http.proxy=socks5h://127.0.0.1:9050 clone --depth=1 http://dacxzjk3kq5mmepbdd3ai2ifynlzxsnpl2cnkfhridqfywihrfftapid.onion/shawn.webb/youtube-dl.git
```

If we only want to grab the repo itself, that's it. If we wanted to contrib and/or push, we need to configure the repo to permanently use our relay.

``` bash
cd youtube-dl
git config --add remote.origin.proxy "socks5h://127.0.0.1:9050"
```

That's it!

## Takeaways

I think `nix` would do well to support tor as part of `std`. Even if they don't take on the policy of adopting packages sourced from tor, giving users the option of adding additional repositories that do return privacy to your operating system would be fantastic, and important to the future of code.

This trend will obviously continue, open source is not compatible with corporate authoritarianism and government statism. We will need increasingly flexible methods to ensure our software, and choices, are still ours.

There's a reason this blog is not hosted on github pages.