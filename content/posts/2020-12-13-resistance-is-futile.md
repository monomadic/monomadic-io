+++
title = "Resistance is futile, and futility will save us."
description = "The future of grey market economies."
category = "economics"
tags = ["economics", "cryptocurrency", "dnm"]
+++
I just finished listening to Aleks Svetski on Peter McCormack’s What Bitcoin Did podcast, and came away feeling exactly like what I imagine Peter did - more questions than answers. With respect to Aleks’ attempt, I think he hasn’t fully thought through a lot of his beliefs about anarchy, borrowing from the idealism of Rothbard and books like The Sovereign Individual, without thinking beyond a simplistic representation of statelessness. I think I came away with the same feeling I suspect Peter had - more unanswered questions and many hypotheticals or logical proofs in place of real answers.

However, I think Peter might have come away unfortunately even more resolved with the idea that the state will just repeat itself over and over, and I’d like to present a case for why that might not be the case.

I am not an anarchist - while I like its literal meaning of ‘no kings’, there is a simple pattern that I notice as a bit of a constant in the development of society - future movements always echo ideas from the past but in a new way that few fully expect. I suspect the same applies to the future of societal and political structure.

Those who are into both bitcoin and economics often understand the significance of separating money from state but usually they are mentioned in a previous chronological context - we once had hard money without debt, then we had soft money with some debt, and now we have very soft money with a great deal of debt. So what happens to a governments ability to regulate others via monetary policy when we abruptly introduce the hardest money ever known? If bitcoins core premise is sovereignty at all costs and imperviousness to state control, does it cause the state to starve and anarchy to replace it?

I don’t think so, but unlike Peter I don’t believe the state can continue in its current fashion and I’d like to propose an alternate to both extremes that could play out instead.

## Contracts
Anarchists seem to love contracts. Some believe contracts can solve all problems, and war will be replaced with contract law enforced by “the threat of violence” via private armies. Not only is this a state, it also sounds like a sucky place for anyone to live who is not rich.

Not only that, to approach this structure requires a tipping of power from a group that have zero coercive control, to suddenly toppling the incumbent power. I don’t see how citizens go from “no power” to “mad max power” even gradually. A series of civil wars is likely to play out like Hong Kongs resistance - brave but futile.

One of the questions asked in this podcast in response to contract control was “who enforces the contracts?” to which the answer was (paraphrasing) “people with guns, lots of guns”. This answer is unsatisfactory for a number of reasons, and I see no reason that previous generations would spend hundreds of years and countless lives trying to restructure themselves away from random acts of war only to fall into an unpredictable system where the one with the most guns wins.

However, I think the question gets turned on its head if you look at the way decentralised governance is not only starting to work, but in some cases it’s working faster and better than human governance. Of course that’s not always the case, and in these early testing-ground days mostly these governance systems are degenerate gamblers trying to squeeze money out of people slightly more stupid than them. But that’s evolution of the idea, and we are still in the primordial soup stage.

I am not saying that MakerDAO or something is going to take over the world, at all. What I am thinking is that these ideas and experiments are not going to kill the system, but slowly replace and improve it. “Improvement by 1000 cuts”. They’re removing the bad parts, the human parts, the opaque parts, the parts that can be short circuited or abused by greed, and replacing them with a truly democratic system that doesn’t always use voting, but instead uses stores of value as the democratic medium.

I’ll come back to this, because I haven’t fully made my point, and right now it sounds like bad white-paper philosophy, but I’ll get there I promise.

## Jill the Dissident
Let me go a little off track here to illustrate a point and propose a ‘what if’ scenario.

In todays society, if a small group of libertarians decided they no longer wished to play by state rules or pay taxes and wanted to create a little colony to themselves, what would the states next action be? Yes, I know it’s already sounding like either a mainstream religion or cult (if there’s a distinction) but lets say it wasn’t, lets say it was a well meaning group of people who wanted to return to agrarianism with no leaders.

Seeing as they want taxes from these people, they have a financial incentive to coerce them with violence. The fact that it is a known location, with physical demarcation, makes it a target, just like the [gold up that poor individuals rectum] last week. The state have all the cards in this scenario and the small village have no protections except the mild threat of a disturbance while the national guard shoot them and the media labels them a bunch of wackos.

Now lets move waaaay into the future, lets say 2024 (see what I did there), and I think we can all assume unless cryptocurrency has been destroyed in some way, its adoption should have expanded.

Lets add a character to the story. Jill, a Brazillian whose government is in the process of collapsing into civil war after the currency rapidly hyper-inflated and citizens were starving in the streets while those close to government and the wealthy class started construction of walled, armed guarded suburbs (which is already happening there by the way).

She has a job at 7-11 where she earns roughly 9c per day in modern terms, but this job is merely a social necessity - being employed gives her social score decent enough to be granted medication for her mother, and allows travel access of 25km, and the benefits are required to keep up appearances and explain why she isn’t starving.

But Jill is a naughty dissident. At night she uses anonymous dark markets and tests software for remote clients in return for private cryptocurrency. Her identity and reputation are stored under a blockchain private key, and people only know her by a hash and the procedural avatar it generates. It signs the messages to her anonymous clients and she is paid to this address. She uses it to purchase more effective medication than the state can provide on dark net markets. Perhaps she also buys other non-illegal every day items and they are delivered to her by private couriers or drones the way drug taxis deliver anonymous drugs in europe these days. These couriers also work for crypto and can sell anything you want, within reason.

Unlike the Brazilian economy, this one is thriving.

The reason I typed out this story is to ask if this kind of setup is likely. Given where we are now, I don’t see why not, in fact, I’d say if you believe that government becomes more restrictive, increases taxation and forces users into their fiat system and strips basic human rights like dissent, privacy and speech, then it’s an inevitability, the question is just ‘when’.

I don’t see why one couldn’t live an entire second life anonymously and privately and away from government control. The currency is not tied to a hyper-inflating government, and it can be used anywhere in the world.

If you haven’t flicked away yet I’ll assume that means you consider it a possibility.

## Value as Governance
If we continue with this thought, what would happen on a larger scale, if these communities started to organise? What if some communities gained enough monetary power, and shared collective vision, that it started to overwhelm the nations they lived in? As the traditional government turned into more of a minarchist structure, providing actual necessities, other economies started to form, at first in the dark, then in the grey, then finally in the light, when government had little power to stop them.

I’m not saying the effect would be the rise of a new cohesive government, or lack of one. What I am asking is, what if open rules (smart contracts anyone can access and audit) all govern tiny constantly experimental parts of society, collectively building a truly democratic environment?

There are countless experiments and approaches to decentralised governance, and they include far more than literal voting through having a governance token. I am not suggesting this is the exact way things will happen, in fact I lean toward cryptocurrency ownership and use itself being more like a vote. I don’t want to hole myself into one particular method of governance, since it could be something totally new to us, but more prefer the basic understanding that decentralised governance is _possible_ on blockchains, and leave the implementation details up to the future.

## But What About...
It’s a big leap at first, and the best way to test an idea is run a bunch of scenarios. Let’s do that.

### Corrupt Police
Say one governance system starts up that acts as a police unit. Everyone believes in it, so they all put money into it. It’s semi-centralised, but the currency that supports it isn’t. If they are racist and target minorities, and racism is not ideal for the country, then people would move their money into a competing project.

When this organisation starts, perhaps it slowly gains trust by being more of a neighborhood watch, proving it can regulate itself until governance votes that they have more power. This power can be taken away if things change.

No reason we can’t have a court system that is funded by exchanged value, for a certain jurisdiction.

### Racist Elites Making Corrupt Police
But lets say we have a system where a few people put a lot of money into an openly racist system. If the MAJORITY don’t like it, the people who actually create equity and wealth through the efforts of their labor, then the racist police end up with money they paid a lot for, but can’t use. It is not valued against other monetary systems.

### Racist Elites Becoming Majority
Okay, say we enter a situation like the USA today, where racial tension is high and some views considered openly racist are now openly discussed and debated issues - a growing percentage of the population actually seem to believe in racial inequity. What happens if the majority actually do want authoritarian rule, or want racism as policy to make a comeback?

I didn’t say this setup had morals, or what is right or wrong, I’m only trying to hypothesize what is likely to actually happen. If the majority of people in a country are racist, then they probably deserve to have that country and be racists. Just like if trump had won his election, democrats would have had to have abided by that. That’s democracy.

## The Open Borders Thing
Which leads me to that old anarchist crux of open borders. Open borders would be required but also rapidly embraced, if this situation were to happen. Collective movements would be more likely to take those who had the money they appreciated, and this would cause a natural gravitation toward each other physically.

Racists who like racist police would bunch into the racist police state country, and those who believe in UBI would migrate to UBIWorld.

## Wow you got this far
Assessing a model for society is only useful if it has a practical method of actually happening. The thing is, the last few decades have shown us that society actually can kick ass if it done right. Life can be truly amazing, especially with freedom to travel to almost any country in the world, we are more free than any other time throughout history. Most people wouldn’t want to risk losing the good things we have, and particularly would not make a conscious decision to do so - societies subconsciously change based on improvements in technology, philosophy, culture and ideas.

When people reject society, if you listen closely they are usually just rejecting the bad parts, the bits that inhibit or offend them personally, and those bad parts invariably stem from the fact that opaque leadership always leads to abuse. Humans will always screw over other humans, especially if nobody is watching, and nobody is accountable. For the first time in our history we have an opportunity to move some, or all, of that power into a series of transparent, democratic systems.

There is this saying that technology, not politics, drives society. I believe this is true. Technology can be a force of oppression but also great liberation.

There’s no reason we can’t have a whole society, with laws, rules, and moderate safety similar to what we have now, only legitimately democratic, without the necessity of a central group of leaders and a broken corrupt electoral system. It’s not anarchy, and it’s not statist. It’s neither and yet it’s both.

I would call this post capitalism, but really, we are already post capitalism, and in a sense isn’t moving post-currency and instead using the merit of ideas as money the most capitalist thing we could possibly accomplish as a species?