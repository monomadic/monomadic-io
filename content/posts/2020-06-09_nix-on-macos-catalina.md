+++
title = "Installing Nix on MacOS Catalina"
description = "Bypassing APFS readonly volume restrictions to install Nix."
category = "NixOS"
tags = ["nixos", "nix", "macos", "catalina", "apfs"]
+++

Apple changed its filesystem recently to APFS, and upon switching to Catalina, also split the system volume into two subpartitions, one being a totally read-only system partition.

This blindsighted the Nix project, as the way it works depends on a writable folder in the root, `/nix`. Executables hard-link themselves to runtimes inside this directory so that they do not need to require anything outside the NixOS system.

There is a way to enable Nix, but it's not documented yet not obvious. It involves synthetic volumes.

First, you need to add a line to the `synthetic.conf` file, 

``` bash
echo 'nix' | sudo tee -a /etc/synthetic.conf
```

Reboot.

``` bash
sudo diskutil apfs addVolume disk1 APFSX Nix -mountpoint /nix
sudo diskutil enableOwnership /nix
sudo chflags hidden /nix  # Don't show the Nix volume on the desktop
echo "LABEL=Nix /nix apfs rw" | sudo tee -a /etc/fstab
```

Then install nix with:
``` bash
sh <(curl https://nixos.org/nix/install) --daemon
```

Note that if you had Nix previously installed, you will have configuration files and so on that the installer may complain about - it is safe to remove them as the underlying system no longer exists.
