{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  name = "typescript-env";

  buildInputs = with pkgs; [
    zola
  ];

  shellHook = "";
}
